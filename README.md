# Usage/instructions
Works best in Chrome, breaks in IE.
OK in other windows browsers.
Untested on Mac/Linux.

Step 1: Double-click "index.html"
Step 2: Profit?

# Changes
Added nice background image and some more or less fancy styling. (Chrome supports fancy "backdrop-filters".)
Did not use the filter method to get the comments of a specific post,
since I found an endpoint that would do it for me.
(Still made use of "URLSearchParams()" to build the url for the "view comments"-links though.)
Added mailto-functionality to the email links.

Pro-tip: Translate the latin for a more fun read. :P