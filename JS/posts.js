// Immediately invoked function expression
const Poster = (function() {
    // Private
    let posts = [];

    // Public
    return {
        getPosts() {
            return fetch("https://jsonplaceholder.typicode.com/posts")
                .then(resp => resp.json());
        },

        createPostTemplate(post) {
            const postDiv = document.createElement("div");
            const postTitle = document.createElement("h4");
            const postBody = document.createElement("p");
            const postLink = document.createElement("a");

            postTitle.innerText = post.title;
            postBody.innerText = `"${post.body}"`;
            postLink.innerText = "View comments";
            postLink.href = `../HTML/comments.html?post=${post.id}`;

            postDiv.appendChild(postTitle);
            postDiv.appendChild(postBody);
            postDiv.appendChild(postLink);

            return postDiv;
        },

        getComments(postId) {
            const queryParam = isNaN(parseInt(postId)) ? "" : `?postId=${postId}`;
            return fetch(`https://jsonplaceholder.typicode.com/comments${queryParam}`)
                .then(resp => resp.json());
        },

        createCommentTemplate(comment) {
            const commentDiv = document.createElement("div");
            const commentName = document.createElement("h4");
            const commentEmail = document.createElement("a");
            const commentBody = document.createElement("p");

            commentName.innerText = comment.name;
            commentEmail.innerText = comment.email;
            commentEmail.href = `mailto:${comment.email}`;
            commentBody.innerText = `"${comment.body}"`;

            commentDiv.appendChild(commentName);
            commentDiv.appendChild(commentBody);
            commentDiv.appendChild(commentEmail);

            return commentDiv;
        }
    }
})();