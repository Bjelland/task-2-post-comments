async function getComments(postId) {

    const elComments = document.getElementById("comments");

    try {
        const postId = new URLSearchParams(window.location.search).get('post');
        const comments = await Poster.getComments(postId);
        comments.forEach(comment => elComments.appendChild(Poster.createCommentTemplate(comment)));
    } catch (e) {
        console.error(e);
    }
}

getComments();